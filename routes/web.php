<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Rute
Route::get('rute', 'RuteController@index')->name('rute.index');
Route::get('rute/create', 'RuteController@create')->name('rute.create');
Route::post('rute/save', 'RuteController@save')->name('rute.save');
Route::get('rute/delete/{route_id}', 'RuteController@delete')->name('rute.delete');
Route::get('rute/edit/{route_id}', 'RuteController@edit')->name('rute.edit');
// Route::patch('rute/{route_id}', 'RuteController@update')->name('rute.update');
// Route::post('rute/update/{route_id}', 'RuteController@update')->name('rute.update');
Route::put('rute/{route_id}', 'RuteController@update')->name('rute.update');

//Transportasi
Route::get('transportasi', 'TransportasiController@index')->name('transportasi.index');
Route::get('transportasi/create', 'TransportasiController@create')->name('transportasi.create');
Route::post('transportasi/save', 'TransportasiController@save')->name('transportasi.save');
Route::get('transportasi/delete/{transportation_id}', 'TransportasiController@delete')->name('transportasi.delete');
Route::get('transportasi/edit/{transportation_id}', 'TransportasiController@edit')->name('transportasi.edit');
// Route::patch('transportasi/{route_id}', 'TransportasiController@update')->name('transportasi.update');
// Route::post('transportasi/update/{route_id}', 'TransportasiController@update')->name('transportasi.update');
Route::put('transportasi/{transportation_id}', 'TransportasiController@update')->name('transportasi.update');

//Kategori
Route::get('kategori', 'KategoriController@index')->name('kategori.index');
Route::get('kategori/create', 'KategoriController@create')->name('kategori.create');
Route::post('kategori/save', 'KategoriController@save')->name('kategori.save');
Route::get('kategori/delete/{category_id}', 'KategoriController@delete')->name('kategori.delete');
Route::get('kategori/edit/{category_id}', 'KategoriController@edit')->name('kategori.edit');
// Route::patch('kategori/{route_id}', 'KategoriController@update')->name('kategori.update');
// Route::post('kategori/update/{route_id}', 'KategoriController@update')->name('kategori.update');
Route::put('kategori/{category_id}', 'KategoriController@update')->name('kategori.update');

//User
Route::get('user', 'UserController@index')->name('user.index');
Route::get('user/create', 'UserController@create')->name('user.create');
Route::post('user/save', 'UserController@save')->name('user.save');
Route::get('user/delete/{user_id}', 'UserController@delete')->name('user.delete');
Route::get('user/edit/{user_id}', 'UserController@edit')->name('user.edit');
// Route::patch('user/{route_id}', 'UserController@update')->name('user.update');
// Route::post('user/update/{route_id}', 'UserController@update')->name('user.update');
Route::put('user/{user_id}', 'UserController@update')->name('user.update');