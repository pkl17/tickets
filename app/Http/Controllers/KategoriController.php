<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
     //Tampilkan data
     public function index(){
        $category = DB::table('category')->paginate(5);
        return view('kategori.indexKategori',['category'=> $category]);
    }

    public function create(){
        return view('kategori.createKategori');
    }

    //Menyimpan data yang sudah di create
    public function save(Request $request){
        // dd($request->all());
        $this->_validation($request);
        DB::table('category')->insert([
            ['category_name'=> $request->category_name]
        ]);
        return redirect()->route('kategori.index')->with('message', 'Successfully Insert Data');
    }

    // Untuk validasi data
    private function _validation(Request $request){
        $validation = $request->validate([
            'category_name' => 'required',
        ]);
    }

    public function edit($category_id){
        $data = DB::table('category')->where('category_id', $category_id)->first();
        return view('kategori.editKategori', compact('data'));
    }

    // Menyimpan data yang sudah di edit
    public function update(Request $request, $category_id){
        // dd($request->all());
        $this->_validation($request);
        DB::table('category')->where('category_id', $category_id)->first()->update([
            ['category_name'=> $request->category_name,]
        ]);
        return redirect()->route('kategori.index')->with('message', 'Successfully Updated Data');
    }

    public function delete($category_id){
        DB::table('category')->where('category_id',$category_id)->delete();
        return redirect()->back()->with('message', 'Successfully Deleted Data');
        
    }
}
