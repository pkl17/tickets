<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //Tampilkan data
    public function index(){
        $user = DB::table('user')->paginate(5);
        return view('user.indexUser',['user'=> $user]);
    }

    public function create(){
        return view('user.createUser');
    }

    //Menyimpan data yang sudah di create
    public function save(Request $request){
        // dd($request->all());
        $this->_validation($request);
        DB::table('user')->insert([
            ['user_name'=> $request->user_name,
            'user_username'=> $request->user_username,
            'user_password'=> $request->user_password,
            'user_level'=> $request->user_level,
            ]
        ]);
        return redirect()->route('user.index')->with('message', 'Successfully Insert Data');
    }

    // Untuk validasi data
    private function _validation(Request $request){
        $validation = $request->validate([
            'user_name' => 'required',
            'user_username' => 'required',
            'user_password' => 'required',
            'user_level' => 'required'
        ]);
    }

    public function edit($user_id){
        $data = DB::table('user')->where('user_id', $user_id)->first();
        return view('user.editUser', compact('data'));
    }

    // Menyimpan data yang sudah di edit
    public function update(Request $request, $user_id){
        // dd($request->all());
        $this->_validation($request);
        DB::table('user')->where('user_id', $user_id)->first()->update([
            ['user_name'=> $request->user_name,
            'user_username'=> $request->user_username,
            'user_password'=> $request->user_password,
            'user_level'=> $request->user_level]
        ]);
        return redirect()->route('user.index')->with('message', 'Successfully Updated Data');
    }

    public function delete($user_id){
        DB::table('user')->where('user_id',$user_id)->delete();
        return redirect()->back()->with('message', 'Successfully Deleted Data');
        
    }
}
