<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransportasiController extends Controller
{
    //Tampilkan data
    public function index(){
        $transportation = DB::table('transportation')->paginate(3);
        return view('transportasi.indexTransportasi',['transportation'=> $transportation]);
    }

    public function create(){
        $category = DB::table('category')-> get();
        return view('transportasi.createTransportasi', ['category'=>$category]);
    }

    //Menyimpan data yang sudah di create
    public function save(Request $request){
        // dd($request->all());
        $this->_validation($request);
        DB::table('transportation')->insert([
            ['transportation_name'=> $request->transportation_name,
             'transportation_code'=> $request->transportation_code,
             'transportation_seat'=> $request->transportation_seat,
             'category_id'=> $request->category_id
            ]
        ]);
        return redirect()->route('transportasi.index')->with('message', 'Successfully Insert Data');
    }

    // Untuk validasi data
    private function _validation(Request $request){
        $validation = $request->validate([
            'transportation_name' => 'required',
            'transportation_code'=> 'required',
            'transportation_seat'=> 'required',
            'category_id'=> 'required|max:2|min:1'
        ]);
    }

    public function edit($transportation_id){
        $data = DB::table('transportation')->where('transportation_id', $transportation_id)->first();
        return view('transportasi.editTransportasi', compact('data'));
    }

    // Menyimpan data yang sudah di edit
    public function update(Request $request, $transportation_id){
        // dd($request->all());
        $this->_validation($request);
        DB::table('transportation')->where('transportation_id', $transportation_id)->first()->update([
            ['transportation_name'=> $request->transportation_name,
            'transportation_code'=> $request->transportation_code,
            'transportation_seat'=> $request->transportation_seat,
            'category_id'=> $request->category_id
           ]
        ]);
        return redirect()->route('transportasi.index')->with('message', 'Successfully Updated Data');
    }

    public function delete($transportation_id){
        DB::table('transportation')->where('transportation_id',$transportation_id)->delete();
        return redirect()->back()->with('message', 'Successfully Deleted Data');
        
    }
}
