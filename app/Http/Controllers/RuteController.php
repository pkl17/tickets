<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RuteController extends Controller
{
    //Tampilkan data
    public function index(){
        $route = DB::table('route')->paginate(5);
        return view('rute.indexRute',['route'=> $route]);
    }

    public function create(){
        $transportation = DB::table('transportation')-> get();
        return view('rute.createRute', ['transportation'=> $transportation]);
    }

    //Menyimpan data yang sudah di create
    public function save(Request $request){
        // dd($request->all());
        $this->_validation($request);
        DB::table('route')->insert([
            ['route_destination'=> $request->route_destination,
             'route_start'=> $request->route_start,
             'route_end'=> $request->route_end,
             'route_price'=> $request->route_price,
             'route_time'=> $request->route_time,
             'transportation_id'=> $request->transportation_id]
        ]);
        return redirect()->route('rute.index')->with('message', 'Successfully Insert Data');
    }

    // Untuk validasi data
    private function _validation(Request $request){
        $validation = $request->validate([
            'route_destination' => 'required|max:300|min:4',
            'route_start'=> 'required|max:300|min:4',
            'route_end'=> 'required|max:300|min:4',
            'route_price'=> 'required|max:5|min:5',
            'transportation_id'=> 'required|max:2|min:1'
        ]);
    }

    public function edit($route_id){
        $data = DB::table('route')->where('route_id', $route_id)->first();
        $transportation = DB::table('transportation')-> get();
        return view('rute.editRute', compact('data'), ['transportation'=> $transportation]);
    }

    // Menyimpan data yang sudah di edit
    public function update(Request $request, $route_id){
        // dd($request->all());
        $this->_validation($request);
        DB::table('route')->where('route_id', $route_id)->first()->update([
            ['route_destination'=> $request->route_destination,
             'route_start'=> $request->route_start,
             'route_end'=> $request->route_end,
             'route_price'=> $request->route_price,
             'route_time'=> $request->route_time,
             'transportation_id'=> $request->transportation_id]
        ]);
        return redirect()->route('rute.index')->with('message', 'Successfully Updated Data');
    }

    public function delete($route_id){
        DB::table('route')->where('route_id',$route_id)->delete();
        return redirect()->back()->with('message', 'Successfully Deleted Data');
        
    }
}
