@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('user.update', $data->user_id)}}" method="POST">
                    @csrf
                    @method('PUT')
                  <div class="form-group">
                    <label 
                      @error('user_name') 
                      class="text-danger" 
                      @enderror> Name
                      @error('user_name')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="user_name" 
                    @if (old('user_name'))
                        value="{{old('user_name')}}"                       
                    @else
                       value="{{$data-> user_name}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Username</label>
                     <input type="text" name="user_username" 
                    @if (old('user_username'))
                        value="{{old('user_username')}}"                       
                    @else
                       value="{{$data-> user_username}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Password</label>
                    <input type="text" name="user_password" 
                    @if (old('user_password'))
                        value="{{old('user_password')}}"                       
                    @else
                       value="{{$data-> user_password}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Level</label>
                    <input type="text" name="user_level" 
                    @if (old('user_level'))
                        value="{{old('user_level')}}"                       
                    @else
                       value="{{$data-> user_level}}"
                    @endif
                    class="form-control">
                  </div>

                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection