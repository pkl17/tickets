@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('user.save')}}" method="POST">
                    @csrf
                  <div class="form-group">
                    <label 
                      @error('user_name') 
                      class="text-danger" 
                      @enderror> Name
                      @error('user_name')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="user_name" class="form-control">
                  </div>

                  <div class="form-group">
                    <label 
                      @error('user_username') 
                      class="text-danger" 
                      @enderror> Username
                      @error('user_username')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="user_username" class="form-control">
                  </div>

                  <div class="form-group">
                    <label 
                      @error('user_password') 
                      class="text-danger" 
                      @enderror> Password
                      @error('user_password')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="user_password" class="form-control">
                  </div>

                  <div class="form-group">
                    <label 
                      @error('user_level') 
                      class="text-danger" 
                      @enderror> Level
                      @error('user_level')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="user_level" class="form-control">
                  </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection