@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <a href="{{route('user.create')}}" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Add User</a>
            <hr>
            @if (session('message'))
            <div class="alert alert-success alert-dismissible show fade">
                <div class="alert-body">
                  <button class="close" data-dismiss="alert">
                    <span>×</span>
                  </button>
                  {{session('message')}}
                </div>
              </div>
            @endif
            {{-- <table class="table">
                <tr>
                    <th>Destination</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Price</th>
                    <th>Time</th>
                    <th>Transportasi</th>
                </tr>
                <tr></tr>
            </table> --}}
            <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-sm">
                      <tbody><tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Level</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                      </tr>
                      @foreach ($user as $no => $data)
                      <tr>
                        <td>{{$user->firstItem()+$no}}</td>
                        <td>{{$data->user_name}}</td>
                        <td>{{$data->user_username}}</td>
                        <td>{{$data->user_level}}</td>
                        <td>{{$data->created_at}}</td>
                        <td>{{$data->updated_at}}</td>
                        <td>
                            <a href="{{route('user.edit', $data->user_id)}}" class="badge badge-icon badge-warning"><i class="far fa-edit"></i></a>
                            <a href="{{route('user.delete', $data->user_id)}}" class="badge badge-icon badge-danger swal-confirm" 
                                onclick="return confirm('Are You Sure?')"><i class="fas fa-exclamation-triangle"></i></i>
                                <form action="{{route('user.delete', $data->user_id)}}" user_id="delete{{$data->user_id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                </form>
        
                            </a>                     
                        </td>
                      </tr>
                      @endforeach

                    </tbody></table>
                    {{$user->links()}}
                  </div>
                </div>
                
              </div>
        </div>
    </div>
</div>

@endsection

@push('page-scripts')
  {{-- <script src="./assets/modules/sweetalert/sweetalert.min.js"></script> --}}
@endpush

@push('after-script')
{{-- <script>
$(".swal-confirm").click(function() {
    route_id = e.target.dataset.route_id;
    swal({
        title: 'Are you sure?'+route_id,
        text: 'Once deleted, you will not be able to recover this imaginary file!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Poof! Your imaginary file has been deleted!', {
          icon: 'success',
        });
        $(`#delete${route_id}`).submit();
        } else {
        swal('Your imaginary file is safe!');
        }
      });
  });
</script>  --}}
@endpush