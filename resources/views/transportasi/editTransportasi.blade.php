@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('transportasi.update', $data->transportation_id)}}" method="POST">
                    @csrf
                    @method('PUT')
                  <div class="form-group">
                    <label 
                      @error('transportation_name') 
                      class="text-danger" 
                      @enderror> Name
                      @error('transportation_name')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="transportation_name" 
                    @if (old('transportation_name'))
                        value="{{old('transportation_name')}}"                       
                    @else
                       value="{{$data-> transportation_name}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Code</label>
                     <input type="text" name="transportation_code" 
                    @if (old('transportation_code'))
                        value="{{old('transportation_code')}}"                       
                    @else
                       value="{{$data-> transportation_code}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Seat</label>
                    <input type="text" name="transportation_seat" 
                    @if (old('transportation_seat'))
                        value="{{old('transportation_seat')}}"                       
                    @else
                       value="{{$data-> transportation_seat}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Category</label>
                    <input type="text" name="category_id" 
                    @if (old('category_id'))
                        value="{{old('category_id')}}"                       
                    @else
                       value="{{$data-> category_id}}"
                    @endif
                    class="form-control">
                  </div>

                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection