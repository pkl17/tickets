@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('transportasi.save')}}" method="POST">
                    @csrf
                  <div class="form-group">
                    <label 
                      @error('transportation_name') 
                      class="text-danger" 
                      @enderror> Name
                      @error('transportation_name')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="transportation_name" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Code</label>
                    <input type="text" name="transportation_code" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Seat</label>
                    <input type="text" name="transportation_seat" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Category</label>
                    <select class="form-control" name="category_id">
                      <option value="">- Pilihan -</option>
                      @foreach ($category as $data)
                      <option value="{{ $data->category_id }}">{{ $data->category_name }}</option>
                      @endforeach
                  </select>
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection