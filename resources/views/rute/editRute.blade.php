@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('rute.update', $data->route_id)}}" method="POST">
                    @csrf
                    @method('PUT')
                  <div class="form-group">
                    <label 
                      @error('route_destination') 
                      class="text-danger" 
                      @enderror> Destination
                      @error('route_destination')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="route_destination" 
                    @if (old('route_destination'))
                        value="{{old('route_destination')}}"                       
                    @else
                       value="{{$data-> route_destination}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Start</label>
                     <input type="text" name="route_start" 
                    @if (old('route_start'))
                        value="{{old('route_start')}}"                       
                    @else
                       value="{{$data-> route_start}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>End</label>
                    <input type="text" name="route_end" 
                    @if (old('route_end'))
                        value="{{old('route_end')}}"                       
                    @else
                       value="{{$data-> route_end}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Price</label>
                    <input type="text" name="route_price" 
                    @if (old('route_price'))
                        value="{{old('route_price')}}"                       
                    @else
                       value="{{$data-> route_price}}"
                    @endif
                    class="form-control">
                  </div>

                  <div class="form-group">
                    <label>Time</label>
                    <input type="text" name="route_time" 
                    @if (old('route_time'))
                        value="{{old('route_time')}}"                       
                    @else
                       value="{{$data-> route_time}}"
                    @endif
                    class="form-control">
                  </div>
                  
                  <div class="form-group">
                    <label>Transportasi</label>
                    <input type="text" name="transportation_id" 
                    @if (old('transportation_id'))
                        value="{{old('transportation_id')}}"                       
                    @else
                       value="{{$data-> transportation_id}}"
                    @endif
                    class="form-control">

                    
                  </div>

                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection