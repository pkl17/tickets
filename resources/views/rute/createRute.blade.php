@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('rute.save')}}" method="POST">
                    @csrf
                  <div class="form-group">
                    <label 
                      @error('route_destination') 
                      class="text-danger" 
                      @enderror> Destination
                      @error('route_destination')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="route_destination" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Start</label>
                    <input type="text" name="route_start" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>End</label>
                    <input type="text" name="route_end" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Price</label>
                    <input type="text" name="route_price" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Time</label>
                    <input type="time" name="route_time" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Transportasi</label>
                    <select class="form-control" name="transportation_id">
                      <option value="">- Pilihan -</option>
                      @foreach ($transportation as $data)
                      <option value="{{ $data->transportation_id }}">{{ $data->transportation_code }}</option>
                      @endforeach
                  </select>
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection