@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('kategori.update', $data->category_id)}}" method="POST">
                    @csrf
                    @method('PUT')
                  <div class="form-group">
                    <label 
                      @error('category_name') 
                      class="text-danger" 
                      @enderror> Destination
                      @error('category_name')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="category_name" 
                    @if (old('category_name'))
                        value="{{old('category_name')}}"                       
                    @else
                       value="{{$data-> category_name}}"
                    @endif
                    class="form-control">
                  </div>

                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection