@extends('layouts.master')
@section('title', 'Tiket Bus')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body">
                <form action="{{route('kategori.save')}}" method="POST">
                    @csrf
                  <div class="form-group">
                    <label 
                      @error('category_name') 
                      class="text-danger" 
                      @enderror> Name
                      @error('category_name')
                        | {{$message}}                          
                      @enderror
                    </label>
                    <input type="text" name="category_name" class="form-control">
                  </div>
                <div class="card-footer text-right">
                  <button class="btn btn-primary mr-1" type="submit">Submit</button>
                  <button class="btn btn-secondary" type="reset">Reset</button>
                </div>
                </form>
              </div>
        </div>
    </div>
</div>

@endsection